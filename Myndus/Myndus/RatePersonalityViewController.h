//
//  RatePersonalityViewController.h
//  Mynd
//
//  Created by Dhruv on 19/08/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "SendMail.h"
@interface RatePersonalityViewController : UIViewController
{
    IBOutlet UISlider *sld_mnd_swr;
    IBOutlet UISlider *sld_mnd_spMs;
    IBOutlet UISlider *sld_mnd_emo;
    IBOutlet UISlider *sld_mnd_lol;
    IBOutlet UISlider *sld_do_swr;
    IBOutlet UISlider *sld_do_spMs;
    IBOutlet UISlider *sld_do_emo;
    IBOutlet UISlider *sld_do_lol;
}

@end
