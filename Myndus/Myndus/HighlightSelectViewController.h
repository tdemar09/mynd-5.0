//
//  HighlightSelectViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/21/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

//#import "HighlighterBaseViewController.h"
#import "HackBaseViewController.h"

@interface HighlightSelectViewController : HackBaseViewController {
    IBOutlet UIView *btnView;
    IBOutlet UIButton *selectBtn;
}

@property (nonatomic, strong) UIView *btnView;
@property (nonatomic, strong) UIButton *selectBtn;

@end
