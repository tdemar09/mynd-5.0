//
//  SleepViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/20/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"

@interface SleepViewController : UIViewController <AVAudioPlayerDelegate> {
    IBOutlet UIButton *pauseButton;
    IBOutlet UIButton *soundbtn1, *soundbtn2, *soundbtn3;
    IBOutlet UIImageView *imgView, *pauseImgView;
    
    NSTimer *timer1, *timer2;
}

@property (nonatomic, strong) UIButton *pauseButton;
@property (nonatomic, strong) UIButton *soundbtn1, *soundbtn2, *soundbtn3;
@property (nonatomic, strong) UIImageView *imgView, *pauseImgView;

@end
