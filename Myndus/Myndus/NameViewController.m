//
//  NameViewController.m
//  Mynd
//
//  Created by Troy DeMar on 9/5/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "NameViewController.h"
//#import "AppDelegate.h"
//#import "UserDefaultsStore.h"
#import "SendMail.h"

@interface NameViewController ()

@end

@implementation NameViewController
@synthesize nameView, optionView;
@synthesize nameTextField;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    UITapGestureRecognizer* tapRecon = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(easterEggAction:)];
//    tapRecon.numberOfTapsRequired = 1;
//    [self.navigationController.navigationBar addGestureRecognizer:tapRecon];
    
    
    
    NSInteger min = 1;
    NSInteger max = 2;
    NSInteger random = min + arc4random_uniform(max - min + 1);
    NSLog(@"Random: %i", random);
}




#pragma mark - UIActionSheetDelegate Methods
/*
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%i", buttonIndex);
    
    if (buttonIndex == 0) {
        [self slowFeedAction:nil];
    }

}
*/




#pragma UITextFieldDelegate Methods

/*
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}
*/



#pragma IBAction Methods
/*
- (IBAction)experiment1Action:(id)sender{
    //            [[UserDefaultsStore shared] setCurrentWeekHackSequence:newSeq forExperiment:Experiment1 count:1];
    
    //Update Experiment to alternate Sequence
    HackSequence newSeq = [[UserDefaultsStore shared] currentWeekHackSequenceForExperiment:Experiment1];
    NSNumber *count = [[UserDefaultsStore shared] currentWeekHackSequenceCountForExperiment:Experiment1];
    int newCount = [count intValue] + 1;
    
    if (newSeq == HackSequenceSleep)
        newSeq = HackSequenceAwake;
    else
        newSeq = HackSequenceSleep;
    
    [[UserDefaultsStore shared] setCurrentWeekHackSequence:newSeq forExperiment:Experiment1 count:newCount];

    
    //Reset Primer Count
    [[UserDefaultsStore shared] setTirednessCount:0];
    [[UserDefaultsStore shared] setPrimerCount:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TiredQuestionController"];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:nav animated:NO completion:^{
        nameView.hidden = NO;
        optionView.hidden = YES;
    }];
}

- (IBAction)experiment2Action:(id)sender{
    //Update Experiment to alternate Sequence
    HackSequence newSeq = [[UserDefaultsStore shared] currentWeekHackSequenceForExperiment:Experiment2];
    NSNumber *count = [[UserDefaultsStore shared] currentWeekHackSequenceCountForExperiment:Experiment2];
    int newCount = [count intValue] + 1;
    
    if (newSeq == HackSequenceSleep)
        newSeq = HackSequenceAwake;
    else
        newSeq = HackSequenceSleep;
    
    [[UserDefaultsStore shared] setCurrentWeekHackSequence:newSeq forExperiment:Experiment2 count:newCount];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SlowFeedController"];
    
    [self presentViewController:vc animated:NO completion:^{
        nameView.hidden = NO;
        optionView.hidden = YES;
    }];
}



- (IBAction)easterEggAction:(id)sender{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Easter Egg" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Try Easter Egg", nil];
    [sheet showInView:self.view];

}


- (IBAction)slowFeedAction:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SlowFeedController"];
    
    [self presentViewController:vc animated:NO completion:nil];

}


- (IBAction)continueAction:(id)sender {
    
    if ([nameTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incomplete" message:@"Please enter your name for use" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    else {
        [nameTextField resignFirstResponder];
        
        
        //Set Username
        [[UserDefaultsStore shared] setUsername:nameTextField.text];

        nameView.hidden = YES;
        optionView.hidden = NO;
    }
    
    //TiredQuestion1Controller
    //TirednessNav
    //SleepRitualNav
}
*/



#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}




@end
