//
//  CommitListViewController.h
//  Myndus
//
//  Created by Troy DeMar on 10/7/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "HackBaseViewController.h"
#import "CommitTableViewCell.h"

@interface CommitListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CommitCellDelegate> {
    IBOutlet UITableView *dataTableView;
    
    CommitTableViewCell *selectedCommitCell;

    
}

@property (strong, nonatomic) UITableView *dataTableView;

@end
