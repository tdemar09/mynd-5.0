//
//  UINavigationController+Orientation.m
//  Myndus
//
//  Created by Troy DeMar on 10/2/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "UINavigationController+Orientation.h"

@implementation UINavigationController (Orientation)

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return YES;
}


@end
