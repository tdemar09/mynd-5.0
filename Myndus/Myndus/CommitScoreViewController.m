//
//  CommitScoreViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "CommitScoreViewController.h"
#import "UserDefaultsStore.h"
#import "NSDate-Utilities.h"
#import "SendMail.h"

@interface CommitScoreViewController ()

@end

@implementation CommitScoreViewController
@synthesize imageView, todayLbl, totalLbl;
@synthesize answerSentence, answerOption;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.hidesBackButton = YES;
    
    self.imageView.image = [[UserDefaultsStore shared] randomScoreImage];

    
    //    for (NSString *sent in chosenWordsArray) {
    //        NSLog(@"Word: %@", sent);
    //    }
    
    [self setupScore];
}


- (void)setupScore{
    
    //Send Result to Email
    //UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
    //NSDictionary *optionDict = [userDefaultsStore unUsedCommitmentDictionary];
//    NSString *answerSentence = nil;
//
//    if (self.answerOption == 0)
//        answerSentence = kCommitStaticAnswer;
//    
//    
//    else if (self.answerOption == 1)
//        answerSentence = [optionDict objectForKey:@"line1"];
//    
//    
//    else if (self.answerOption == 2)
//        answerSentence = [optionDict objectForKey:@"line2"];
//    
//    else if (self.answerOption == 3)
//        answerSentence = [optionDict objectForKey:@"line3"];
    
//    SendMail *sendMail = [SendMail new];
//    [sendMail sendMessageInBack:self.answerSentence];
//    NSLog(@"Attempting to send email: %@", self.answerSentence);
    
    
    
    
    
    //generate today score USE HIGHLIGHTER SCORE
    NSInteger score = [[UserDefaultsStore shared] generateHighlighterScore];
    
    //save to total
    [[UserDefaultsStore shared] incrementHighlighterScore:score];
    
    [todayLbl setText:[NSString stringWithFormat:@"%i", score]];
    [totalLbl setText:[NSString stringWithFormat:@"%i", [[UserDefaultsStore shared] highlighterTotal]]];
    
    //[[UserDefaultsStore shared] commitHackChosen];
}


//- (void)



#pragma mark - IBAction Methods

- (IBAction)continueAction:(id)sender{
//    if ([[UserDefaultsStore shared] isImprinterDoneToday]){
//        //Used in 2nd set of Hacks, of which is the last, so return to Main
//        [[NSNotificationCenter defaultCenter] postNotificationName:kReturnToMainNotif object:nil];
//    }
    
    [self.navigationController popToRootViewControllerAnimated:NO];


}

@end
