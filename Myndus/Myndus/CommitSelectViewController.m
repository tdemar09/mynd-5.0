//
//  CommitSelectViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/22/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "CommitSelectViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "CommitDisplayViewController.h"
#import "UserDefaultsStore.h"

@interface CommitSelectViewController ()
@property (nonatomic, strong) NSString *chosenString;
@property (nonatomic) NSInteger chosenOption;
@end

@implementation CommitSelectViewController
@synthesize naButton;
@synthesize viewOption;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"commitDisplaySegue"] || [segue.identifier isEqualToString:@"commitDisplayTwoSegue"]) {
        
        
        CommitDisplayViewController *displayController = (CommitDisplayViewController *)segue.destinationViewController;
        displayController.answerOption = self.chosenOption;
        displayController.viewOption = self.viewOption;

    }
    
    
    if ([segue.identifier isEqualToString:@"commitSelectTwoSegue"]) {
        CommitSelectViewController *selectController = (CommitSelectViewController *)segue.destinationViewController;
        selectController.viewOption = 1;
        
    }

}



- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.instrImageView.image = [UIImage imageNamed:@"HackIntro_Commit.png"];
    
    if (![[UserDefaultsStore shared] isCommitmentInstrDone]) {
        [[UserDefaultsStore shared] setCommitmentInstrDone:YES];
    }else {
        self.instrView.hidden = YES;
    }
    
    
    //View 0 or 1 Option
    if (!self.viewOption)
        viewOption = 0;
    
    
    
    [self displayOptions];
    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 300, 320, 145)];
//    imgView.image = [[UserDefaultsStore shared] commitmentSigImage];
//    [self.view addSubview:imgView];
    
    
    [self updateBaseLayouts];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}




- (void)displayOptions{
    UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
    NSDictionary *optionDict = [userDefaultsStore unUsedCommitmentDictionary];
    NSArray *percArray = [[UserDefaultsStore shared] commitmentPercentagesArray];
    BOOL plusOnTop = [[UserDefaultsStore shared] isCommitmentPlusOnTop];
    //NSString *noneSentence = @"I will not do any of these things.\nI will do nothing.";
    //NSLog(@"Percentages: %@", percArray);


    CGFloat spaceY = 50;

    //Static Option 0
    UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [noBtn setFrame:CGRectMake(10, spaceY, 300, 55)];
    
    [noBtn setTag:0];
    [noBtn setBackgroundImage:[UIImage imageNamed:@"Hack_Commitment_Grey"] forState:UIControlStateNormal];
    [noBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    //[noBtn setTitle:@"I will not do any if these things.\nI will do nothing." forState:UIControlStateNormal];
    [noBtn setAttributedTitle:[self attribSentenceForComment:kCommitStaticAnswer withPercentage:[[percArray objectAtIndex:0] stringValue]] forState:UIControlStateNormal];
    noBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [noBtn.titleLabel setFont:[UIFont systemFontOfSize:19]];
    [noBtn addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:noBtn];
    
    
    
    //Option 1
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setFrame:CGRectMake(10, noBtn.frame.origin.y+noBtn.frame.size.height+spaceY,
                              300, 2+[self textViewHeightForAttributedText:[self attribTextForComment:[optionDict objectForKey:@"line1"]] andWidth:290])]; //269
    
    [btn1 setTag:1];
    [btn1 setBackgroundImage:[UIImage imageNamed:@"Hack_Commitment_Blue"] forState:UIControlStateNormal];
    [btn1 setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    //[btn1 setTitle:[optionDict objectForKey:@"line1"] forState:UIControlStateNormal];
    [btn1 setAttributedTitle:[self attribSentenceForComment:[optionDict objectForKey:@"line1"] withPercentage:[[percArray objectAtIndex:1] stringValue]] forState:UIControlStateNormal];
    btn1.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [btn1.titleLabel setFont:[UIFont systemFontOfSize:19]];
    [btn1 addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    
    
    //Option 2
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setFrame:CGRectMake(10, btn1.frame.origin.y+btn1.frame.size.height+spaceY,
                              300, 2+[self textViewHeightForAttributedText:[self attribTextForComment:[optionDict objectForKey:@"line2"]] andWidth:290])];
    
    [btn2 setTag:2];
    [btn2 setBackgroundImage:[UIImage imageNamed:@"Hack_Commitment_Red"] forState:UIControlStateNormal];
    [btn2 setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    //[btn2 setTitle:[optionDict objectForKey:@"line2"] forState:UIControlStateNormal];
    [btn2 setAttributedTitle:[self attribSentenceForComment:[optionDict objectForKey:@"line2"] withPercentage:[[percArray objectAtIndex:2] stringValue]] forState:UIControlStateNormal];
    btn2.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [btn2.titleLabel setFont:[UIFont systemFontOfSize:19]];
    [btn2 addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn2];
    
    
    
    //Option3
    [self.naButton setFrame:CGRectMake(btn2.frame.origin.x, btn2.frame.origin.y+btn2.frame.size.height+spaceY, btn2.frame.size.width, btn2.frame.size.height)];
    [self.naButton setTag:3];
    if ([optionDict objectForKey:@"line3ImgName"]) {
        self.naButton.hidden = NO;
        [self.naButton setBackgroundImage:[UIImage imageNamed:[optionDict objectForKey:@"line3ImgName"]] forState:UIControlStateNormal];
    }
    
    
    
    //Plus Button
    UIImageView *plusImgView;
    if (plusOnTop)
        plusImgView = [[UIImageView alloc] initWithFrame:CGRectMake(255, btn1.frame.origin.y+btn1.frame.size.height-15, 30, 30)];
    else
        plusImgView = [[UIImageView alloc] initWithFrame:CGRectMake(255, btn2.frame.origin.y+btn2.frame.size.height-10, 30, 30)];
    
    plusImgView.image = [UIImage imageNamed:@"Plus"];
    [self.view addSubview:plusImgView];
    
    
    
    //Button Color Options
    if (self.viewOption == 1) {
        [btn1 setBackgroundImage:[UIImage imageNamed:@"Commit_Green"] forState:UIControlStateNormal];
        [btn2 setBackgroundImage:[UIImage imageNamed:@"Commit_Yellow"] forState:UIControlStateNormal];


    }

}


- (NSMutableAttributedString *)attribSentenceForComment:(NSString *)aComment withPercentage:(NSString *)percentage{
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    NSMutableAttributedString *attrSentence = [[NSMutableAttributedString alloc] initWithString:aComment
                                                                     attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont systemFontOfSize:19]}];
    
    
    NSMutableAttributedString *attrPercentage = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@%%", percentage]
                                                                       attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:19]}];
    
    [attrSentence appendAttributedString:attrPercentage];
    
    return attrSentence;
}


- (NSAttributedString *)attribTextForComment:(NSString *)aComment{
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    NSAttributedString *attribText = [[NSAttributedString alloc] initWithString:aComment
                                                                     attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:19], NSParagraphStyleAttributeName: paragraphStyle}];
    
    return attribText;
}


- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    
    
    //NSLog(@"Text Height: %f", size.height);
    return size.height;
}


- (void)vibrate{
    AudioServicesPlaySystemSound (1352);
}




#pragma mark - IBAction Methods

- (IBAction)continueAction:(id)sender{
    UIButton *button = (UIButton *)sender;
    
    [self vibrate];
    
    self.chosenOption = button.tag;
   
//    if (self.viewOption == 0) {
//        CommitSelectViewController *commitSelectController = [self.storyboard instantiateViewControllerWithIdentifier:@"CommitSelectController"];
//        commitSelectController.viewOption = 1;
//        [self.navigationController pushViewController:commitSelectController animated:YES];
//    }
//    else
//        [self performSegueWithIdentifier:@"rotate1Segue" sender:nil];
    
    
    if (self.viewOption == 0)
        [self performSegueWithIdentifier:@"commitDisplaySegue" sender:nil];
    else
        [self performSegueWithIdentifier:@"commitDisplayTwoSegue" sender:nil];
}

- (IBAction)showInstrAction:(id)sender{
    [super showInstrAction:sender];
    
}



@end
