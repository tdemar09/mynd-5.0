//
//  CommitDisplayViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "CommitDisplayViewController.h"
#import "CommitSelectViewController.h"
#import "CommitScoreViewController.h"
#import "NSDate-Utilities.h"
#import "UserDefaultsStore.h"
#import "SendMail.h"

@interface CommitDisplayViewController ()
@property (nonatomic, strong) NSString *answerSentence;
@property (nonatomic) int answerImageOpt;
@end

@implementation CommitDisplayViewController
@synthesize answerImgView, viewOption, answerOption;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"commitSelectTwoSegue"]) {
        CommitSelectViewController *selectController = (CommitSelectViewController *)segue.destinationViewController;
        selectController.viewOption = 1;
        
    }
    
    else if ([segue.identifier isEqualToString:@"commitScoreSegue"]) {
        CommitScoreViewController *scoreController = (CommitScoreViewController *)segue.destinationViewController;
        scoreController.answerOption = self.answerOption;
        scoreController.answerSentence = self.answerSentence;
    }
    
    
    
}




- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //self.navigationItem.hidesBackButton = YES;
    self.instrView.hidden = YES;
    
    [self displayAnswer];
    [self saveAnswer];
}




- (NSAttributedString *)attribTextForComment:(NSString *)aComment{
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    NSAttributedString *attribText = [[NSAttributedString alloc] initWithString:aComment
                                                                     attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:19], NSParagraphStyleAttributeName: paragraphStyle}];
    
    return attribText;
}


- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    
    
    //NSLog(@"Text Height: %f", size.height);
    return size.height;
}


- (void)displayAnswer{
    UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
    NSDictionary *optionDict = [userDefaultsStore unUsedCommitmentDictionary];
    UIImage *image1, *image2;
    
    if (self.viewOption == 0) {
        image1 = [UIImage imageNamed:@"Hack_Commitment_Shapes_triangleA"];
        image2 = [UIImage imageNamed:@"Hack_Commitment_Shapes_triangleB"];
    } else {
        image1 = [UIImage imageNamed:@"Hack_Commitment_Shapes_squareA"];
        image2 = [UIImage imageNamed:@"Hack_Commitment_Shapes_squareB"];
    }
    
    if (self.answerOption == 0){
        NSInteger random = 1 + arc4random_uniform(2 - 1 + 1);
        if (random == 1) {
            answerImgView.image = image1;
            
            if (self.viewOption == 0)
                self.answerImageOpt = 0;
            else
                self.answerImageOpt = 2;
        }
        else {
            answerImgView.image = image2;
            
            if (self.viewOption == 0)
                self.answerImageOpt = 1;
            else
                self.answerImageOpt = 3;
        }
        
        self.answerSentence = kCommitStaticAnswer;
    }
    
    else if (self.answerOption == 1){
        answerImgView.image = image1;
        self.answerSentence = [optionDict objectForKey:@"line1"];
        
        if (self.viewOption == 0)
            self.answerImageOpt = 0;
        else
            self.answerImageOpt = 2;
    }
    
    else if (self.answerOption == 2){
        answerImgView.image = image2;
        self.answerSentence = [optionDict objectForKey:@"line2"];
        
        if (self.viewOption == 0)
            self.answerImageOpt = 1;
        else
            self.answerImageOpt = 3;
    }
    
    else if (self.answerOption == 3){
        NSInteger random = 1 + arc4random_uniform(2 - 1 + 1);
        if (random == 1) {
            answerImgView.image = image1;
            
            if (self.viewOption == 0)
                self.answerImageOpt = 0;
            else
                self.answerImageOpt = 2;
        }
        
        else {
            answerImgView.image = image2;
            
            if (self.viewOption == 0)
                self.answerImageOpt = 1;
            else
                self.answerImageOpt = 3;
        }
        
        self.answerSentence = [optionDict objectForKey:@"line3"];

    }
    
    
//    NSLog(@"Otpion Dic: %@", optionDict);
//    if ([[optionDict objectForKey:@"notification"] boolValue]) {
//        NSLog(@"YES");
//    }
//    else NSLog(@"NO");
    
    
    //Answer Button
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setFrame:CGRectMake(10, answerImgView.frame.origin.y+answerImgView.frame.size.height+30,
                              300, 2+[self textViewHeightForAttributedText:[self attribTextForComment:self.answerSentence] andWidth:290])]; //269
    
    //[btn1 setBackgroundImage:[UIImage imageNamed:@"Hack_Commitment_Blue"] forState:UIControlStateNormal];
    [btn1 setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    [btn1 setTitle:self.answerSentence forState:UIControlStateNormal];
    btn1.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn1.titleLabel setFont:[UIFont systemFontOfSize:19]];
    [btn1 setEnabled:NO];
    [self.view addSubview:btn1];
    btn1.layer.borderColor = [UIColor blackColor].CGColor;
    btn1.layer.borderWidth = 1.0f;
    
    
    //Signature
    UIImageView *sigImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, btn1.frame.origin.y+btn1.frame.size.height + 5, 320, 100)];
    sigImgView.image = [[UserDefaultsStore shared] commitmentSigImage];
    [self.view addSubview:sigImgView];
    
    //sigImgView.layer.borderColor = [UIColor blackColor].CGColor;
    //sigImgView.layer.borderWidth = 1.0f;
    
}


- (void)saveAnswer{
    UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
    NSDictionary *optionDict = [userDefaultsStore unUsedCommitmentDictionary];
    
    //Save only if options 1 or 2
    NSString *answer = nil;
    NSNumber *jacks = nil;
    
    if (self.answerOption == 1 || self.answerOption == 2) {
        answer = self.answerSentence;
        
        //Check for jumping jacks
        if ([optionDict objectForKey:@"jacks1"] && [optionDict objectForKey:@"jacks2"]) {
            //Commitment has 2 jumping jacks options
            answer = nil;
            
            if (self.answerOption == 1)
                jacks = [optionDict objectForKey:@"jacks1"];
            else if (self.answerOption == 2)
                jacks = [optionDict objectForKey:@"jacks2"];
        }
        
        else {
            //Has only 1 or None jumping jacks options
            if (self.answerOption == 1) {
                answer = [optionDict objectForKey:@"line1"];
                jacks = [optionDict objectForKey:@"jacks1"];
            }
            else if (self.answerOption == 2) {
                answer = [optionDict objectForKey:@"line1"];
                jacks = [optionDict objectForKey:@"jacks2"];
            }
        }
        
    }
    
    
    
    
    //1st Commitment of the day
    if (![[UserDefaultsStore shared] isCommitmentDoneToday]) {
        [[UserDefaultsStore shared] commitHackChoseOption:answer imageOption:self.answerImageOpt jacksNumber:jacks];
    }
    
    //2nd Commitment of the day
    else {
        [[UserDefaultsStore shared] commitHackChoseOption:answer imageOption:self.answerImageOpt jacksNumber:jacks];
        [[UserDefaultsStore shared] commitment2Done];
    }
    
    
    //Local Notification
    if ((self.answerOption == 1) && [[optionDict objectForKey:@"notification"] boolValue]) {
        //NSLog(@"notification");
        
        NSDate *inBedDate = [[UserDefaultsStore shared] prescribedInBedDate];
        if (inBedDate) {
            //NSLog(@"In bed date");
            NSDate *today = [NSDate date];
            
            NSDateComponents *tiredComps = [[NSDateComponents alloc] init];
            [tiredComps setMonth:[today month]];
            [tiredComps setDay:[today day]];
            [tiredComps setYear:[today year]];
            [tiredComps setHour:[inBedDate hour]];
            [tiredComps setMinute:[inBedDate minute]];
            
            NSDate *bedTime = [[NSCalendar currentCalendar] dateFromComponents:tiredComps];
            NSNumber *hrs = [optionDict objectForKey:@"notificationHrs"];
            NSDate *bedTime2 = [bedTime dateBySubtractingHours:[hrs intValue]];
            
            //Schedule Notification
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = bedTime2;
            notification.repeatInterval = NSDayCalendarUnit;
            notification.alertBody = [optionDict objectForKey:@"notificationString"];
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            
            //NSLog(@"Secheduled Notification: %@", [optionDict objectForKey:@"notificationString"]);
            //NSLog(@"Scheduled Time: %@", bedTime.description);
            //NSLog(@"Notifcation Time: %@", bedTime2.description);

        }
    }
    
    [self email];
    
}


- (void)email{
//    SendMail *sendMail = [SendMail new];
//    [sendMail sendMessageInBack:self.answerSentence];
//    NSLog(@"Attempting to send email: %@", self.answerSentence);
}




#pragma mark - IBAction Methods

- (IBAction)showInstrAction:(id)sender{
    self.instrView.hidden = YES;
}



@end
