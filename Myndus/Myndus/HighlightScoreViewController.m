//
//  HighlightScoreViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/22/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "HighlightScoreViewController.h"
#import "UserDefaultsStore.h"

@interface HighlightScoreViewController ()

@end

@implementation HighlightScoreViewController
@synthesize imageView, todayLbl, totalLbl;
@synthesize chosenWordsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.instrImageView.hidden = YES;
    
    
    self.imageView.image = [[UserDefaultsStore shared] randomScoreImage];
    [self setupScore];
}


- (void)setupScore{
    //generate today score
    NSInteger score = [[UserDefaultsStore shared] generateHighlighterScore];
    
    //save to total
    [[UserDefaultsStore shared] incrementHighlighterScore:score];
    
    [todayLbl setText:[NSString stringWithFormat:@"%i", score]];
    [totalLbl setText:[NSString stringWithFormat:@"%i", [[UserDefaultsStore shared] highlighterTotal]]];
    
    
//    UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
//    if (![userDefaultsStore isHighlighterDoneToday])
//        [userDefaultsStore highlighterHackChoseWords:chosenWordsArray];
//    
//    else {
//        [userDefaultsStore highlighterHackChoseWords:chosenWordsArray];
//        [userDefaultsStore highlighter2Done];
//    }
    
}




#pragma mark - IBAction Methods

- (IBAction)continueAction:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
