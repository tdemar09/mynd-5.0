//
//  SleepRitualGameViewController.m
//  Mynd
//
//  Created by Troy DeMar on 8/14/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "SleepRitualGameViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "AppDelegate.h"
#import "UserDefaultsStore.h"

const int end1 = 40; //40

@interface SleepRitualGameViewController ()
@property (strong, nonatomic) NSMutableArray *graphArray, *edgeArray;
@property (strong, nonatomic) NSMutableArray *bedArray, *bedInverseArray;
@property (strong, nonatomic) CircleNode *mainNode, *tailNode1, *tailNode2;
@property (strong, nonatomic) CircleNode *touchedNode;
@property (nonatomic) NSInteger score, touchCount, nodeMoves;
@end

@implementation SleepRitualGameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    NSLog(@"dealloc");
    [secTimer invalidate];
    [timer invalidate];
    [btmLeftTimer invalidate];
    [bedTimer invalidate];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear");
    
    [secTimer invalidate];
    [timer invalidate];
    [btmLeftTimer invalidate];
    [bedTimer invalidate];
}



- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //NSLog(@"viewDidLoad");
    
    if (![[UserDefaultsStore shared] isSleepRitualInstrDone]) {
        
        if (self.sequence == HackSequenceInterventionSleep) {
            self.instrImageView.image = [UIImage imageNamed:@"Hack_Ritual_Intro_Night.png"];
        }
        else if (self.sequence == HackSequenceInterventionWake) {
            self.instrImageView.image = [UIImage imageNamed:@"Hack_Ritual_Intro_Day.png"];
        }
        
        [[UserDefaultsStore shared] setSleepRitualInstrDone:YES];
    }
    else {
        instrView.hidden = YES;
        [self performSelector:@selector(start) withObject:nil afterDelay:1.0f];
    }
    
    
    if (self.sequence == HackSequenceInterventionSleep) {
        interval = 0.25;
    }
    else if (self.sequence == HackSequenceInterventionWake) {
        interval = 2.0f/3.0f;
    }
    
    
    
    
    
    self.bedArray = [@[[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                      [NSNumber numberWithInt:2], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1],
                      [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1],
                       [NSNumber numberWithInt:2], [NSNumber numberWithInt:2]] mutableCopy];
    
    
    self.bedInverseArray = [@[[NSNumber numberWithInt:2], [NSNumber numberWithInt:2], [NSNumber numberWithInt:1],
                       [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1],
                       [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:1],
                       [NSNumber numberWithInt:0], [NSNumber numberWithInt:0]] mutableCopy];
    
    
    [self setupBoard];
    
    [self connectNodes];
    
    self.nodeMoves = 0;
    self.score = 0;
    self.touchCount = 0;
    
    
    [self updateBaseLayouts];

    
    
    //Get Start Speed
//    HackSequence seq = [[UserDefaultsStore shared] currentWeekHackSequenceForExperiment:Experiment1];
//    if (seq == HackSequenceSleep)
//        interval = 0.25;
//    else
//        interval = 2.0f/3.0f;
    
    

}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");

    //NSLog(@"Height: %f", self.view.frame.size.height);
    

    
    

    
    //NSLog(@"Edge Array count: %i", self.edgeArray.count);
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear");

    //[self performSelector:@selector(start) withObject:nil afterDelay:1.0];
}


- (void)setupBoard{
    self.graphArray = [[NSMutableArray alloc] init];
    self.edgeArray = [[NSMutableArray alloc] init];
    
    CGFloat widthLeft = self.view.frame.size.width - (6*45);
    widthLeft = widthLeft/7;
    
    
    //CGFloat x = widthLeft;
    //CGFloat y = 20;
    
    
    //20, 90, 160, 230
    
    for (int col=0; col<6; col++) {
        NSMutableArray *colArray = [[NSMutableArray alloc] init];
        
        
        CGFloat x = widthLeft + (col * (widthLeft + 45));
        
        for (int i=0; i<6; i++) {
            CGFloat y = 90 + (i * (widthLeft + 45));
            //NSLog(@"(%f,%f)", x,y);
            
            CircleNode *circle = [[CircleNode alloc] initWithFrame:CGRectMake(x, y, 45, 45)];
            circle.x = col;
            circle.y = i;
            if (self.sequence == HackSequenceInterventionWake)
                circle.seq = HackSequenceAwake;
            else
                circle.seq = HackSequenceSleep;
                
            circle.delegate = self;
            [self.view addSubview:circle];
            
            [colArray addObject:circle];
            
            
            //Edge Array
            if (col == 0) {
                [self.edgeArray addObject:circle];
                
            } else if (col == 5){
                [self.edgeArray addObject:circle];

            } else {
                if (i==0) {
                    [self.edgeArray addObject:circle];
                    
                } else if (i==5) {
                    [self.edgeArray addObject:circle];
                    
                }
            }
        }
        
        [self.graphArray addObject:colArray];
    }
    
//    NSLog(@"Graph Array Count: %i", self.graphArray.count);
//    for (NSArray *array in self.graphArray) {
//        NSLog(@"Array: %@", array);
//    }
}


- (void)connectNodes{
    for (int col = 0; col<self.graphArray.count; col++) {
        
        NSArray *colArray = [self.graphArray objectAtIndex:col];
        
        
        for (int row = 0; row<colArray.count; row++) {
            CircleNode *node = [colArray objectAtIndex:row];

            
            if (row!=0) {
                //All Tops
                node.top = [colArray objectAtIndex:row-1];
                
                
                if (col != 0) {
                    //Top Left
                    NSArray *lCol = [self.graphArray objectAtIndex:col-1];
                    node.topLeft = [lCol objectAtIndex:row-1];
                }
                
                if (col != self.graphArray.count-1) {
                    //Top Right
                    NSArray *rCol = [self.graphArray objectAtIndex:col+1];
                    node.topRight = [rCol objectAtIndex:row-1];
                }
                
            }
            
            if (row != colArray.count-1) {
                //All Bottoms
                node.bottom = [colArray objectAtIndex:row+1];
                
                
                if (col != 0) {
                    //Bottom Left
                    NSArray *lCol = [self.graphArray objectAtIndex:col-1];
                    node.bottomLeft = [lCol objectAtIndex:row+1];
                }
                
                if (col != self.graphArray.count-1) {
                    //Bottom Right
                    NSArray *rCol = [self.graphArray objectAtIndex:col+1];
                    node.bottomRight = [rCol objectAtIndex:row+1];
                }
            }
            
            
            if (col != 0) {
                //All Lefts
                NSArray *leftArray = [self.graphArray objectAtIndex:col-1];
                node.left = [leftArray objectAtIndex:row];
            }
            
            if (col != self.graphArray.count -1) {
                //All Rights
                NSArray *rightArray = [self.graphArray objectAtIndex:col+1];
                node.right = [rightArray objectAtIndex:row];
            }
            
            
            
        }
        
        
    }
}


- (void)start{
    NSLog(@"start");
    
    //pick random node
    NSInteger min = 0;
    NSInteger max = 19;
    int ran = min + arc4random_uniform(max - min + 1);
    
    
    self.mainNode = [self.edgeArray objectAtIndex:ran];
    //[self.mainNode flickOn:YES];
    [self.mainNode colorMain];
    
    //interval = 0.25;    //Starts 0.25 End 2/3
    seconds = 0;
    
    
    //Get Increase or Decrease
    if (self.sequence == HackSequenceInterventionSleep)

        secTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(decreaseSpeed) userInfo:nil repeats:YES];
    else
        secTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(increaseSpeed) userInfo:nil repeats:YES];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(moveToNextNode) userInfo:nil repeats:NO];
}


- (void)reset{
    [secTimer invalidate];
    [timer invalidate];
    [btmLeftTimer invalidate];
    [bedTimer invalidate];
    
    
    if (self.sequence == HackSequenceInterventionSleep) {
        interval = 0.25;
    }
    else if (self.sequence == HackSequenceInterventionWake) {
        interval = 2.0f/3.0f;
    }
    
    self.nodeMoves = 0;
    self.score = 0;
    self.touchCount = 0;
    
    
    [self.mainNode highlightAllConnections:NO];
    self.mainNode = nil;
    
    [self.tailNode1 highlightAllConnections:NO];
    self.tailNode1 = nil;
    
    [self.tailNode2 highlightAllConnections:NO];
    self.tailNode2 = nil;
}




//Awake; Top Left; Bed Inverse
- (void)increaseSpeed{
    NSLog(@"increase Speed");
    seconds++;
    
    CGFloat dec = ((2.0f/3.0f) - 0.25f) / 40;       // /40
    interval = interval-dec;
    
    //NSLog(@"%f", 2.0f/3.0f);
    NSLog(@"%i, dec: %f;  interval: %f", seconds, dec, interval);
    
    if (seconds == end1) {    //40
        [secTimer invalidate];
        [timer invalidate];
        
        
        //Goto Top Left
        btmLeftTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(moveToTopLeft) userInfo:nil repeats:YES];
        
    }
}

//Sleep; Bottom Left; Bed
- (void)decreaseSpeed{
    NSLog(@"decreaseSpeed");

    seconds++;
    
    CGFloat dec = ((2.0f/3.0f) - 0.25f) / 40;       // /40
    interval = interval+dec;
    
    //NSLog(@"%f", 2.0f/3.0f);
    NSLog(@"%i, dec: %f;  interval: %f", seconds, dec, interval);
    
    if (seconds == end1) {    //40
        [secTimer invalidate];
        [timer invalidate];
        
        
        //Goto Bottom Left
        btmLeftTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(moveToBottomLeft) userInfo:nil repeats:YES];

    }
}



//Distance between nodes
- (int)distanceFromStartNode:(CircleNode *)startNode toDestNode:(CircleNode *)destNode{
    if (startNode == nil || destNode == nil)
        return 0;
    
    int value = 0;
    
    while (startNode != destNode) {
        //Dest is Lower
        if (destNode.y > startNode.y) {
            //NSLog(@"Lower");
            //To the Left
            if (destNode.x < startNode.x) {
                startNode = startNode.bottomLeft;
                //NSLog(@"Left");
                
            }
            
            //To the Right
            else if(destNode.x > startNode.x) {
                startNode = startNode.bottomRight;
                //NSLog(@"Right");
                
            }
            
            //Just Down
            else {
                startNode = startNode.bottom;
                //NSLog(@"Down");
                
            }
        }
        
        //Dest is Higher
        else if (destNode.y < startNode.y) {
            //NSLog(@"Higher");
            
            
            //To the Left
            if (destNode.x < startNode.x) {
                startNode = startNode.topLeft;
                //NSLog(@"Left");
                
            }
            
            //To the Right
            else if(destNode.x > startNode.x) {
                startNode = startNode.topRight;
                //NSLog(@"Right");
                
            }
            
            //Just Up
            else {
                startNode = startNode.top;
                //NSLog(@"Up");
                
            }
        }
        
        //Dest is Even
        else {
            //NSLog(@"Even");
            
            //To the Left
            if (destNode.x < startNode.x) {
                startNode = startNode.left;
                //NSLog(@"Left");
                
            }
            
            //To the Right
            else if(destNode.x > startNode.x) {
                startNode = startNode.right;
                //NSLog(@"Right");
                
            }
            
            
        }
        
        NSLog(@"Start: (%i,%i); End: (%i,%i)", startNode.x, startNode.y, destNode.x, destNode.y);
        NSLog(@"----------------------------");
        
        value++;
    }
    
    
    
    return value;
}

//Move occured, check the distance
- (void)pollDistFromMove{
    if (self.touchedNode != nil) {
        self.score = self.score + [self distanceFromStartNode:self.touchedNode toDestNode:self.tailNode2];
        self.touchCount++;
        NSLog(@"Score: %i", self.score);
    }
    
    self.touchedNode = nil;
}



- (void)moveToNextNode{
    //Poll for Score
    [self pollDistFromMove];

    
    //Tail2 Resets
    [self.tailNode2 colorNormal];
    
    
    //Move both tails up * recolor
    self.tailNode2 = self.tailNode1;
    [self.tailNode2 colorTail2];
    
    self.tailNode1 = self.mainNode;
    [self.tailNode1 colorTail1];
    
    
    //Pick next Node
    BOOL found = NO;
    while (!found) {
        int rand = 0 + arc4random_uniform(3 - 0 + 1);   //removed the option to move diagonal
        
        if (rand == 0) {
            //top
            if (self.mainNode.top != nil && self.mainNode.top != self.tailNode1 && self.mainNode.top != self.tailNode2) {
                self.mainNode = self.mainNode.top;
                found = YES;
            }
        }
        else if (rand == 1) {
            //right
            if (self.mainNode.right != nil && self.mainNode.right != self.tailNode1 && self.mainNode.right != self.tailNode2) {
                self.mainNode = self.mainNode.right;
                found = YES;
            }
        }
        else if (rand == 2) {
            //bottom
            if (self.mainNode.bottom != nil && self.mainNode.bottom != self.tailNode1 & self.mainNode.bottom != self.tailNode2) {
                self.mainNode = self.mainNode.bottom;
                found = YES;
            }
        }
        else if (rand == 3) {
            //left
            if (self.mainNode.left != nil && self.mainNode.left != self.tailNode1 && self.mainNode.left != self.tailNode2) {
                self.mainNode = self.mainNode.left;
                found = YES;
            }
        }
        
        else if (rand == 4) {
            //top left
            if (self.mainNode.topLeft != nil && self.mainNode.topLeft != self.tailNode1 && self.mainNode.topLeft != self.tailNode2) {
                self.mainNode = self.mainNode.topLeft;
                found = YES;
            }
        }
        else if (rand == 5) {
            //top right
            if (self.mainNode.topRight != nil && self.mainNode.topRight != self.tailNode1 && self.mainNode.topRight != self.tailNode2) {
                self.mainNode = self.mainNode.topRight;
                found = YES;
            }
        }
        else if (rand == 6) {
            //bottom left
            if (self.mainNode.bottomLeft != nil && self.mainNode.bottomLeft != self.tailNode1 && self.mainNode.bottomLeft != self.tailNode2) {
                self.mainNode = self.mainNode.bottomLeft;
                found = YES;
            }
        }
        else if (rand == 7) {
            //bottom right
            if (self.mainNode.bottomRight != nil && self.mainNode.bottomRight != self.tailNode1 && self.mainNode.bottomRight != self.tailNode2) {
                self.mainNode = self.mainNode.bottomRight;
                found = YES;
            }
        }
        
        
    }
    
    [self.mainNode colorMain];
    //self.mainNode.backgroundColor = [UIColor orangeColor];
    //NSLog(@"Node: (%f,%f)", self.highlightedNode.frame.origin.)
    
    
    if ([self.tailNode2 isColoredTail]) {
        self.nodeMoves++;
        //NSLog(@"NodeMove: %i", self.nodeMoves);
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(moveToNextNode) userInfo:nil repeats:NO];
}


//Bed Inverse
- (void)moveToTopLeft{
    //Poll for Score
    [self pollDistFromMove];
    
    
    
    //found top left
    if (self.mainNode.left == nil && self.mainNode.top == nil) {
        //NSLog(@"Hit Bottom Left");
        //[btmLeftTimer invalidate];
        
        if (self.tailNode2 != nil) {
            [self.mainNode colorTail1];
            [self.tailNode1 colorTail2];
            [self.tailNode2 colorNormal];
            
            self.tailNode2 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else if (self.tailNode1 != nil) {
            [self.mainNode colorTail2];
            [self.tailNode1 colorNormal];
            
            self.tailNode1 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else{
            [btmLeftTimer invalidate];
            [self.mainNode colorMain];
            
            //Draw Bed
            bedTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(drawBedInverse) userInfo:nil repeats:YES];
            
        }
        return;
    }
    
    
    //Tail2 Resets
    [self.tailNode2 colorNormal];
    
    
    //Move both tails up & recolor
    self.tailNode2 = self.tailNode1;
    [self.tailNode2 colorTail2];
    
    self.tailNode1 = self.mainNode;
    [self.tailNode1 colorTail1];
    
    
    BOOL found = NO;
    
    
    if (self.mainNode.left != nil && self.mainNode.left != self.tailNode1 && self.mainNode.left != self.tailNode2) {
        //left
        self.mainNode = self.mainNode.left;
        found = YES;
    }
    
    else if (self.mainNode.top != nil && self.mainNode.top != self.tailNode1 && self.mainNode.top != self.tailNode2) {
        //top
        self.mainNode = self.mainNode.top;
        found = YES;
    }
    
    else if (self.mainNode.bottom != nil && self.mainNode.bottom != self.tailNode1 & self.mainNode.bottom != self.tailNode2) {
        //bottom
        self.mainNode = self.mainNode.bottom;
        found = YES;
    }
    
    else if (self.mainNode.right != nil && self.mainNode.right != self.tailNode1 && self.mainNode.right != self.tailNode2) {
        //right
        self.mainNode = self.mainNode.right;
        found = YES;
    }

    
    [self.mainNode colorMain];
    
    
    if ([self.tailNode2 isColoredTail]) {
        self.nodeMoves++;
        //NSLog(@"NodeMove: %i", self.nodeMoves);
    }
}

//Bed
- (void)moveToBottomLeft{
    //Poll for Score
    [self pollDistFromMove];
    
    
    //found bottom left
    if (self.mainNode.left == nil && self.mainNode.bottom == nil) {
        //NSLog(@"Hit Bottom Left");
        //[btmLeftTimer invalidate];
        
        if (self.tailNode2 != nil) {
            [self.mainNode colorTail1];
            [self.tailNode1 colorTail2];
            [self.tailNode2 colorNormal];
            
            self.tailNode2 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else if (self.tailNode1 != nil) {
            [self.mainNode colorTail2];
            [self.tailNode1 colorNormal];
            
            self.tailNode1 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else{
            [btmLeftTimer invalidate];
            [self.mainNode colorMain];
            
            //Draw Bed
            bedTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(drawBed) userInfo:nil repeats:YES];

        }
        return;
    }
    
    
    //Tail2 Resets
    [self.tailNode2 colorNormal];
    
    
    //Move both tails up & recolor
    self.tailNode2 = self.tailNode1;
    [self.tailNode2 colorTail2];
    
    self.tailNode1 = self.mainNode;
    [self.tailNode1 colorTail1];
    
    
    BOOL found = NO;

    
    if (self.mainNode.left != nil && self.mainNode.left != self.tailNode1 && self.mainNode.left != self.tailNode2) {
        //left
        self.mainNode = self.mainNode.left;
        found = YES;
    }
    
    else if (self.mainNode.bottom != nil && self.mainNode.bottom != self.tailNode1 & self.mainNode.bottom != self.tailNode2) {
        //bottom
        self.mainNode = self.mainNode.bottom;
        found = YES;
    }
    
    else if (self.mainNode.right != nil && self.mainNode.right != self.tailNode1 && self.mainNode.right != self.tailNode2) {
        //right
        self.mainNode = self.mainNode.right;
        found = YES;
    }
    
    else if (self.mainNode.top != nil && self.mainNode.top != self.tailNode1 && self.mainNode.top != self.tailNode2) {
        //top
        self.mainNode = self.mainNode.top;
        found = YES;
    }
    
    
    [self.mainNode colorMain];

    //found bottom left
//    if (self.mainNode.left == nil && self.mainNode.bottom == nil) {
//        [btmLeftTimer invalidate];
//        
//        //Dismiss Tail
//        //[self.tailNode1 colorNormal];
//        //[self.tailNode2 colorNormal];
//
//        
//        //Draw Bed
//        //bedTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(drawBed) userInfo:nil repeats:YES];
//    }
    
    
    if ([self.tailNode2 isColoredTail]) {
        self.nodeMoves++;
        //NSLog(@"NodeMove: %i", self.nodeMoves);
    }
}


- (void)drawBed{
    //NSLog(@"Draw Bed");
    
    //found bottom right
    if (self.bedArray.count == 0) {
        //NSLog(@"Hit Bottom Right");
        
        //Poll for Score
        [self pollDistFromMove];
        
        
        if (self.tailNode2 != nil) {
            [self.mainNode colorTail1];
            [self.tailNode1 colorTail2];
            [self.tailNode2 colorMain];
            
            self.tailNode2 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else if (self.tailNode1 != nil) {
            [self.mainNode colorTail2];
            [self.tailNode1 colorMain];
            
            self.tailNode1 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else{
            [bedTimer invalidate];
            [self.mainNode colorMain];
            
            [self vibrate];
        }
        return;
    }
    
    
    else if (self.bedArray.count > 0) {
        //Poll for Score
        [self pollDistFromMove];
        
        
        //Tail2 Resets
        [self.tailNode2 colorMain];
        
        
        //Move both tails up & recolor
        self.tailNode2 = self.tailNode1;
        [self.tailNode2 colorTail2];
        
        self.tailNode1 = self.mainNode;
        [self.tailNode1 colorTail1];
        
        
        NSNumber *num = [self.bedArray firstObject];
                
        if ([num intValue] == 0) {
            //top
            self.mainNode = self.mainNode.top;

        }
        else if ([num intValue] == 1) {
            //right
            self.mainNode = self.mainNode.right;

        }
        else if ([num intValue] == 2) {
            //bottom
            self.mainNode = self.mainNode.bottom;

        }
        else if ([num intValue] == 3) {
            //left
            self.mainNode = self.mainNode.left;

        }
        
        [self.mainNode colorMain];


        
        //Remove Move
        if (self.bedArray.count > 0)
            [self.bedArray removeObjectAtIndex:0];
        

        
        if ([self.tailNode2 isColoredTail]) {
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
    }
    else
        [bedTimer invalidate];

}


- (void)drawBedInverse{
    //NSLog(@"Draw Bed");

    
    //found top right
    if (self.bedInverseArray.count == 0) {
        //NSLog(@"Hit Top Right");
        
        //Poll for Score
        [self pollDistFromMove];
        
        
        if (self.tailNode2 != nil) {
            [self.mainNode colorTail1];
            [self.tailNode1 colorTail2];
            [self.tailNode2 colorMain];
            
            self.tailNode2 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else if (self.tailNode1 != nil) {
            [self.mainNode colorTail2];
            [self.tailNode1 colorMain];
            
            self.tailNode1 = nil;
            
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        else{
            [bedTimer invalidate];
            [self.mainNode colorMain];
            
            [self vibrate];
        }
        return;
    }
    
    
    else if (self.bedInverseArray.count > 0) {
        //Poll for Score
        [self pollDistFromMove];
        
        //Tail2 Resets
        [self.tailNode2 colorMain];
        
        
        //Move both tails up & recolor
        self.tailNode2 = self.tailNode1;
        [self.tailNode2 colorTail2];
        
        self.tailNode1 = self.mainNode;
        [self.tailNode1 colorTail1];
        
        
        NSNumber *num = [self.bedInverseArray firstObject];
        
        if ([num intValue] == 0) {
            //top
            self.mainNode = self.mainNode.top;
            
        }
        else if ([num intValue] == 1) {
            //right
            self.mainNode = self.mainNode.right;
            
        }
        else if ([num intValue] == 2) {
            //bottom
            self.mainNode = self.mainNode.bottom;
            
        }
        else if ([num intValue] == 3) {
            //left
            self.mainNode = self.mainNode.left;
            
        }
        
        [self.mainNode colorMain];
        
        
        
        //Remove Move
        if (self.bedInverseArray.count > 0)
            [self.bedInverseArray removeObjectAtIndex:0];
        
        
        if ([self.tailNode2 isColoredTail]) {
            self.nodeMoves++;
            //NSLog(@"NodeMove: %i", self.nodeMoves);
        }
        
    }
    else
        [bedTimer invalidate];
    
}





- (void)vibrate{
    NSLog(@"Touches/Count: %i/%i", self.touchCount,self.nodeMoves);
    //NSLog(@"Moves: %i", self.nodeMoves);

    AudioServicesPlaySystemSound (1352);
    
    //Save Score in App Delegate
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate setSnakeAccuracy:self.score];
    [delegate setSnakeTailMoves:self.nodeMoves];
    [delegate setSnakeTouches:self.touchCount];
    
    
    //Move to next Screen
    [self performSelector:@selector(continueToScore) withObject:nil afterDelay:0.5];
}


- (void)continueToScore{
    //[self performSegueWithIdentifier:@"sleepRitualScoreSegue" sender:nil];
    
    
    [[UserDefaultsStore shared] sleepRitualDone];

    
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ImprinterBaseController"];
//    
//    [self.navigationController pushViewController:controller animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}








- (void)test{
    
    circle1 = [[CircleNode alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    [self.view addSubview:circle1];
    
    
    circle2 = [[CircleNode alloc] initWithFrame:CGRectMake(80, 10, 50, 50)];
    [self.view addSubview:circle2];
    
    
    circle3 = [[CircleNode alloc] initWithFrame:CGRectMake(150, 10, 50, 50)];
    [self.view addSubview:circle3];
    
    
    circle4 = [[CircleNode alloc] initWithFrame:CGRectMake(220, 10, 50, 50)];
    [self.view addSubview:circle4];
    
    
    //nodes
    circle1.right = circle2;
    
    circle2.left = circle1;
    circle2.right = circle3;
    
    circle3.left = circle2;
    circle3.right = circle4;
    
    circle4.left = circle3;
}


- (void)highlightNode:(CircleNode *)node{
    
}


#pragma mark - IBAction Methods

- (IBAction)instrPlayAction:(id)sender{
    [super instrPlayAction:sender];
    
    
    [self performSelector:@selector(start) withObject:nil afterDelay:1.0];
}

- (IBAction)showInstrAction:(id)sender{
    
    [self reset];
    
    if (self.sequence == HackSequenceInterventionSleep) {
        self.instrImageView.image = [UIImage imageNamed:@"Hack_Ritual_Intro_Night.png"];
    }
    else if (self.sequence == HackSequenceInterventionWake) {
        self.instrImageView.image = [UIImage imageNamed:@"Hack_Ritual_Intro_Day.png"];
    }
    
    [super showInstrAction:sender];
}



- (IBAction)allOff:(id)sender{
    [self.mainNode highlightAllConnections:NO];

}

- (IBAction)test1:(id)sender{
    //pick random node
    int ranCol = 0 + arc4random_uniform(5 - 0 + 1);
    int ranRow = 0 + arc4random_uniform(5 - 0 + 1);

    NSArray *rowArray = [self.graphArray objectAtIndex:ranCol];
    self.mainNode = [rowArray objectAtIndex:ranRow];
    self.mainNode.backgroundColor = [UIColor orangeColor];
    
//    //Highlight Nodes
//    [self highlightNode:self.highlightedNode];
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(moveToNextNode) userInfo:nil repeats:NO];
}

- (IBAction)test2:(id)sender{
    for (NSArray *rowArray in self.graphArray) {
        for (CircleNode *node in rowArray) {
            [node highlightAllConnections:NO];
        }
    }
}

- (IBAction)test3:(id)sender{
    for (CircleNode *node in self.edgeArray) {
        [node flickOn:YES];
    };
}

- (IBAction)test4:(id)sender{
    [circle4 highlightConnectedOn:YES];
}




#pragma mark - CircleNodeDelegate Methods

- (void)touchedDownCircleNode:(CircleNode *)node{
    //NSLog(@"touchedCircleNode");
//    NSLog(@"(%i,%i)", node.x, node.y);
//    
//    if (node == self.tailNode2) {
//        NSLog(@"It's the Tail2");
//    }
//    
//    
//    NSLog(@"Distance: %i", [self distanceFromStartNode:node toDestNode:self.tailNode2]);
    
    self.touchedNode = node;
}


- (void)touchedUpCircleNode:(CircleNode *)node{
    //self.touchedNode = nil;
}




@end





