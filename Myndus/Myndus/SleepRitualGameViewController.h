//
//  SleepRitualGameViewController.h
//  Mynd
//
//  Created by Troy DeMar on 8/14/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleNode.h"
#import "HackBaseViewController.h"

@interface SleepRitualGameViewController : HackBaseViewController <CircleNodeDelegate>{
    CircleNode *circle1, *circle2, *circle3, *circle4;
    
    NSTimer *secTimer, *timer, *btmLeftTimer, *bedTimer;
    NSTimeInterval interval;
    
    int seconds;
}

@end
