//
//  HackBaseViewController.h
//  Myndus
//
//  Created by Troy DeMar on 10/1/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HackBaseViewController : UIViewController <UIActionSheetDelegate> {
    IBOutlet UIView *instrView;
    IBOutlet UIImageView *instrImageView;
    IBOutlet UIButton *instrBtn;
    IBOutlet UISlider *navSlider;
}

@property (strong, nonatomic) UIView *instrView, *navView;
@property (strong, nonatomic) UIImageView *instrImageView;
@property (strong, nonatomic) UIButton *instrBtn;
@property (strong, nonatomic) UISlider *navSlider;
@property (nonatomic) HackSequence sequence;
@property (nonatomic) BOOL repeatSeq;

- (IBAction)instrPlayAction:(id)sender;
- (IBAction)repeatSequence:(id)sender;
- (IBAction)showInstrAction:(id)sender;

- (void)updateBaseLayouts;

@end
