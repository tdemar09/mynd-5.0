//
//  UINavigationController+Orientation.h
//  Myndus
//
//  Created by Troy DeMar on 10/2/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Orientation)

-(NSUInteger)supportedInterfaceOrientations;
-(BOOL)shouldAutorotate;

@end
