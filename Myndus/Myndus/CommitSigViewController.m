//
//  CommitSigViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "CommitSigViewController.h"
#import "PPSSignatureView.h"
#import "UserDefaultsStore.h"

@interface CommitSigViewController ()

@end

@implementation CommitSigViewController
@synthesize containerSigViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //NSLog(@"prepareForSegue");
    if ([segue.identifier isEqualToString:@"embedCommitSigContainer"]) {
        self.containerSigViewController = segue.destinationViewController;
    }
    
    else if ([segue.identifier isEqualToString:@"commitStartAfterSigSegue"]) {
        
    }
}



- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //self.instrImageView.image = [UIImage imageNamed:@"HackIntro_Commit.png"];
    self.instrView.hidden = YES;
}




#pragma mark - IBAction Methods

- (IBAction)continueAction:(id)sender{
    PPSSignatureView *sigView = (PPSSignatureView *)self.containerSigViewController.view;
    
    if ([sigView hasSignature]) {
        
        //Save Image
        UIImage *sigImg = [sigView signatureImage];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"CommitSignature.jpg"];
        [UIImagePNGRepresentation(sigImg) writeToFile:filePath atomically:YES];
        
        
        //Continue
        [self performSegueWithIdentifier:@"commitStartAfterSigSegue" sender:nil];
    }
    else {
        
    }
    
    
}

@end
