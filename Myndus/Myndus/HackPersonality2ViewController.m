//
//  HackPersonality2ViewController.m
//  Mynd
//
//  Created by Dhruv on 19/08/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "HackPersonality2ViewController.h"
#import "UserDefaultsStore.h"

@interface HackPersonality2ViewController ()

@end

@implementation HackPersonality2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    instrImageView.image = [UIImage imageNamed:@"HackIntro_Personality.png"];

    if (![[UserDefaultsStore shared] isPersonalityInstrDone]) {
        [[UserDefaultsStore shared] setPersonalityInstrDone:YES];
    }else {
        instrView.hidden = YES;
    }
    
//    self.arr_SecondFiveQ = [[UserDefaultsStore shared] personalityQuestions];
//    
//    lbl_PersonQ1.text=[self.arr_SecondFiveQ objectAtIndex:0];
//    lbl_PersonQ2.text=[self.arr_SecondFiveQ objectAtIndex:1];
//    lbl_PersonQ3.text=[self.arr_SecondFiveQ objectAtIndex:2];
//    lbl_PersonQ4.text=[self.arr_SecondFiveQ objectAtIndex:3];
//    lbl_PersonQ5.text=[self.arr_SecondFiveQ objectAtIndex:4];
    // Do any additional setup after loading the view.
    
    NSArray *arr_Of_Questions = [[UserDefaultsStore shared] personalityQuestions];
    
    int NoOfCount=[[[NSUserDefaults standardUserDefaults]valueForKey:kPersonalityCountKey]intValue];
    NSLog(@"kPersonalityCountKey: %i", NoOfCount);
    

    if(NoOfCount+4>=arr_Of_Questions.count){
        int i=0;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:kPersonalityCountKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NoOfCount=0;
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",NoOfCount+5] forKey:kPersonalityCountKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    NSLog(@"NoOfCount: %i", NoOfCount);
    NSLog(@"Text1: %@", [arr_Of_Questions objectAtIndex:0]);
    
    lbl_PersonQ1.text=[arr_Of_Questions objectAtIndex:NoOfCount];
    lbl_PersonQ2.text=[arr_Of_Questions objectAtIndex:NoOfCount+1];
    lbl_PersonQ3.text=[arr_Of_Questions objectAtIndex:NoOfCount+2];
    lbl_PersonQ4.text=[arr_Of_Questions objectAtIndex:NoOfCount+3];
    lbl_PersonQ5.text=[arr_Of_Questions objectAtIndex:NoOfCount+4];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Click Event
- (IBAction)ClickEvent1Yes:(id)sender
{
    [self MakeITSelectedYes:btn_1_Yes MakeNO:btn_1_No];
    btn_1_No.backgroundColor=[UIColor clearColor];
    btn_1_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent2Yes:(id)sender
{
    [self MakeITSelectedYes:btn_2_Yes MakeNO:btn_2_No];
    btn_2_No.backgroundColor=[UIColor clearColor];
    btn_2_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
    
}
- (IBAction)ClickEvent3Yes:(id)sender
{
    [self MakeITSelectedYes:btn_3_Yes MakeNO:btn_3_No];
    btn_3_No.backgroundColor=[UIColor clearColor];
    btn_3_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
    
}
- (IBAction)ClickEvent4Yes:(id)sender
{
    [self MakeITSelectedYes:btn_4_Yes MakeNO:btn_4_No];
    btn_4_No.backgroundColor=[UIColor clearColor];
    btn_4_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
    
}
- (IBAction)ClickEvent5Yes:(id)sender
{
    [self MakeITSelectedYes:btn_5_Yes MakeNO:btn_5_No];
    btn_5_No.backgroundColor=[UIColor clearColor];
    btn_5_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent1No:(id)sender
{
    [self MakeITSelectedYes:btn_1_No MakeNO:btn_1_Yes];
    btn_1_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_1_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent2No:(id)sender
{
    [self MakeITSelectedYes:btn_2_No MakeNO:btn_2_Yes];
    btn_2_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_2_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent3No:(id)sender
{
    [self MakeITSelectedYes:btn_3_No MakeNO:btn_3_Yes];
    btn_3_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_3_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent4No:(id)sender
{
    [self MakeITSelectedYes:btn_4_No MakeNO:btn_4_Yes];
    btn_4_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_4_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent5No:(id)sender
{
    [self MakeITSelectedYes:btn_5_No MakeNO:btn_5_Yes];
    btn_5_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_5_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
-(void)MakeITSelectedYes:(UIButton *)btn1 MakeNO:(UIButton *)btn2
{
    btn1.selected=YES;
    btn2.selected=NO;
}
-(void)CheckForTheCondition{
    if (btn_1_Yes.selected||btn_1_No.selected)
    {
        
    }
    else
        return;
    if (btn_2_Yes.selected||btn_2_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_3_Yes.selected||btn_3_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_4_Yes.selected||btn_4_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_5_Yes.selected||btn_5_No.selected)
    {
        
    }
    else
        return;
    
    btn_Continue.enabled=YES;
    
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"]) {
        int i=1;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
    else
    {
        int i=[[[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"] intValue];
        i++;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
    
    NSDateFormatter *df=[[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd-MM-yyyy"];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setLocale:[NSLocale systemLocale]];
    if ([self isItFirstDay])
    {
        [[NSUserDefaults standardUserDefaults]setObject:[df stringFromDate:[NSDate date]] forKey:@"isDoneFirstPL"];
        [self performSegueWithIdentifier:@"OnFirst" sender:nil];
    }
    else
    {
        if (![[NSUserDefaults standardUserDefaults]valueForKey:@"isDoneSecondPL"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[df stringFromDate:[NSDate date]] forKey:@"isDoneSecondPL"];
        }
        [self performSegueWithIdentifier:@"AfterFirst" sender:nil];
    }
}
/*
 
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)ClickedBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)isItFirstDay
{
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"isDoneFirstPL"])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
-(IBAction)EyeClick:(id)sender
{
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
