//
//  AppDelegate.m
//  Myndus
//
//  Created by Troy DeMar on 9/30/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "AppDelegate.h"
#import "SleepRitualGameViewController.h"
#import "UserDefaultsStore.h"

@implementation AppDelegate
@synthesize sliderNumber;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    // Override point for customization after application launch.
#if TARGET_IPHONE_SIMULATOR
    // where are you?
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    NSLog(@"Preferences Directory: Same Devices Directory/data/Library/Preferences/");
#endif
    
    
    //Diable Idle Timer
    application.idleTimerDisabled = YES;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    NSLog(@"applicationDidBecomeActive");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if (![[UserDefaultsStore shared] isToday]) {
        NSLog(@"No longer today, restart");
        [[UserDefaultsStore shared] setToday];
        
        if (self.window.rootViewController.presentedViewController != nil) {
            NSLog(@"Dismiss Modal");
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        }
        
        
        UINavigationController *nav = (UINavigationController *)self.window.rootViewController;
        [nav popToRootViewControllerAnimated:NO];
    }
    
    
    
    

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




- (UISlider *)mainSlider{
    if (self.slider == nil) {
        self.slider = [[UISlider alloc] initWithFrame:CGRectMake(95, 5, 180, 30)];
        self.slider.minimumValue = 0;
        self.slider.maximumValue = 100;
        self.sliderNumber = [NSNumber numberWithFloat:50];
        [self.slider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];
        NSLog(@"Create slider");
    }
    else {
        self.slider.value = [self.sliderNumber floatValue];
        NSLog(@"Existing Slider");
    }
    
    return self.slider;
}

- (UISlider *)newSlider{
    NSLog(@"newSlider");
    
    UISlider *mainSlider = [self mainSlider];
    
    UISlider *aSlider = [[UISlider alloc] initWithFrame:CGRectMake(mainSlider.frame.origin.x, mainSlider.frame.origin.y, mainSlider.frame.size.width, mainSlider.frame.size.height)];
    
    aSlider.minimumValue = mainSlider.minimumValue;
    aSlider.maximumValue = mainSlider.maximumValue;
    [aSlider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];

    
    return aSlider;
}


- (void)loadSequence:(HackSequence)sequence{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    if (sequence == HackSequenceInterventionWake) {
        UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"InterventionNav"];
        SleepRitualGameViewController *ritaulController = (SleepRitualGameViewController *)[nav.viewControllers objectAtIndex:0];
        ritaulController.sequence = HackSequenceInterventionWake;
        
        [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }
    
    else if (sequence == HackSequenceInterventionSleep) {
        //InterventionSleep
        UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"InterventionNav"];
        SleepRitualGameViewController *ritaulController = (SleepRitualGameViewController *)[nav.viewControllers objectAtIndex:0];
        ritaulController.sequence = HackSequenceInterventionSleep;
        
        [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }
    
    
    else if (sequence == HackSequenceSleep) {
        //Sleep
        UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"SleepNav"];
        
        [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }
    
    else if (sequence == HackSequenceCommitList) {
        //Commitment List
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CommitListController"];
        
        [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
        
    }
}


- (void)presentSequence:(HackSequence)sequence{

    if (self.window.rootViewController.presentedViewController != nil) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            
            [self loadSequence:sequence];
        }];
    
    } else {
        [self loadSequence:sequence];
    }
}




- (IBAction)sliderChangeAction:(id)sender{
    NSLog(@"sliderChangeAction");
    self.sliderNumber = [NSNumber numberWithFloat:self.slider.value];
}


@end
