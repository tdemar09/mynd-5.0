//
//  CommitTableViewCell.m
//  Myndus
//
//  Created by Troy DeMar on 10/7/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "CommitTableViewCell.h"
#import "UserDefaultsStore.h"

@implementation CommitTableViewCell
@synthesize textView, imgView, checkImgView, cellBtn, checkBtn;
@synthesize index, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)displayCurrent{
    if (![self isCommitmentCheckedToday]) {
        self.imgView.alpha = 1.0f;
        self.textView.alpha = 1.0f;
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        self.checkBtn.hidden = YES;
    }
    
    else {
        self.imgView.alpha = 0.5f;
        self.textView.alpha = 0.5f;
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
        self.checkBtn.hidden = NO;
    }
    
    [self.contentView addSubview:self.cellBtn];
}



- (BOOL)isCommitmentCheckedToday{
    NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
    NSDictionary *dict = [commitsArray objectAtIndex:self.index];
    
    
    NSDate *saveDate = [dict objectForKey:@"checkedTodayDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}



- (IBAction)cellButtonHit:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    if (![self isCommitmentCheckedToday]) {
        //Check Image
        if (self.checkBtn.hidden) {
            
            self.checkBtn.hidden = NO;
            [self.contentView addSubview:self.checkBtn];
            
        }
        else {
            self.checkBtn.hidden = YES;
            [self.contentView addSubview:self.cellBtn];
        }
    }
    
    else {
        //Make appearance normal & display check
        self.imgView.alpha = 1.0f;
        self.textView.alpha = 1.0f;
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        
        [self.contentView addSubview:self.checkBtn];
    }
    
    
    if (delegate && [delegate respondsToSelector:@selector(commitCell:didTapCellButton:atIndex:)]) {
        [delegate commitCell:self didTapCellButton:btn atIndex:self.index];
    }
}


- (IBAction)checkButtonHit:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    if (![self isCommitmentCheckedToday]) {
        //Update to Checked
        NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
        NSMutableDictionary *dict = [[commitsArray objectAtIndex:self.index] mutableCopy];
        
        [dict setObject:[NSDate date] forKey:@"checkedTodayDate"];
        [[UserDefaultsStore shared] setCommitmentDict:dict atIndex:self.index];
        
        //Change Cell Images
        self.imgView.alpha = 0.5f;
        self.textView.alpha = 0.5f;
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
        //self.userInteractionEnabled = NO;
    }
    
    else {
        //Update to Un-Check
        NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
        NSMutableDictionary *dict = [[commitsArray objectAtIndex:self.index] mutableCopy];
        
        [dict removeObjectForKey:@"checkedTodayDate"];
        [[UserDefaultsStore shared] setCommitmentDict:dict atIndex:self.index];
        
        //Change Cell Images
        self.imgView.alpha = 1.0f;
        self.textView.alpha = 1.0f;
        [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        self.checkBtn.hidden = YES;
    }
    
    
    
    if (delegate && [delegate respondsToSelector:@selector(commitCell:didTapCheckButton:atIndex:)]) {
        [delegate commitCell:self didTapCheckButton:btn atIndex:self.index];
    }
}

@end
