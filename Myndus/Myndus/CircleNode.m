//
//  CircleNode.m
//  Mynd
//
//  Created by Troy DeMar on 8/14/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "CircleNode.h"

@implementation CircleNode
@synthesize top, left, right, bottom;
@synthesize topLeft, topRight, bottomLeft, bottomRight;
@synthesize x, y, seq;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //self.backgroundColor = [UIColor whiteColor];
        //self.layer.cornerRadius = self.bounds.size.width/2.0;
        
        imgView = [[UIImageView alloc] initWithFrame:self.bounds];
        imgView.image = [UIImage imageNamed:@"BallBlack.png"];
        [self addSubview:imgView];

        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:self.bounds];
        //[btn addTarget:self action:@selector(turnNodeConnectionsOn:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn addTarget:self action:@selector(nodeTouchDown:) forControlEvents:UIControlEventTouchDown];
        [btn addTarget:self action:@selector(nodeTouchUp:) forControlEvents:UIControlEventTouchUpInside];

        
        [self addSubview:btn];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - IBAction Methods

- (IBAction)turnNodeConnectionsOn:(id)sender{
    [self highlightAllConnections:YES];
}



- (IBAction)nodeTouchDown:(id)sender{
    if (delegate && [delegate respondsToSelector:@selector(touchedDownCircleNode:)]) {
        [delegate touchedDownCircleNode:self];
    }
}

- (IBAction)nodeTouchUp:(id)sender{
    if (delegate && [delegate respondsToSelector:@selector(touchedUpCircleNode:)]) {
        [delegate touchedUpCircleNode:self];
    }
}


#pragma mark - Public Methods

- (void)colorNormal{
    imgView.image = [UIImage imageNamed:@"BallBlack.png"];
    isTail = NO;
}

- (void)colorMain{
    if (seq == HackSequenceAwake)
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Wake_Dot1.png"];
    else
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Sleep_Dot1.png"];
    
    isTail = NO;
}

- (void)colorTail1{
    if (seq == HackSequenceAwake)
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Wake_Dot2.png"];
    else
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Sleep_Dot2.png"];
    
    isTail = NO;
}

- (void)colorTail2{
    if (seq == HackSequenceAwake)
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Wake_Dot3.png"];
    else
        imgView.image = [UIImage imageNamed:@"Hack_Ritual_Sleep_Dot3.png"];
    
    isTail = YES;
}


- (BOOL)isColoredTail{
    return isTail;
}


- (void)highlightAllConnections:(BOOL)on{
    [top flickOn:on];
    [left flickOn:on];
    [right flickOn:on];
    [bottom flickOn:on];
    
    [topLeft flickOn:on];
    [topRight flickOn:on];
    [bottomLeft flickOn:on];
    [bottomRight flickOn:on];
}




- (void)flickOn:(BOOL)on{
    if (on)
        [self colorMain];
    else
        [self colorNormal];

}


- (void)highlightConnectedOn:(BOOL)on{
    [top flickOn:on];
    [left flickOn:on];
    [right flickOn:on];
    [bottom flickOn:on];
}

@end
