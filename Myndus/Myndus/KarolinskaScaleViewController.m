//
//  KarolinskaScaleViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/2/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "KarolinskaScaleViewController.h"

@interface KarolinskaScaleViewController ()

@end

@implementation KarolinskaScaleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];

    
    self.instrView.hidden = YES;
    //self.navView.hidden = YES;

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];


}




#pragma mark - IBAction Methods

- (IBAction)showInstrAction:(id)sender{
    self.instrView.hidden = YES;
}

- (IBAction)scaleAction:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [UIView animateWithDuration:0.2 animations:^{
        [btn setBackgroundColor:[UIColor lightGrayColor]];
        [btn setAlpha:0.5f];
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            [btn setBackgroundColor:[UIColor clearColor]];
            [btn setAlpha:1];

        } completion:^(BOOL finished) {
            if (self.sequence == HackSequenceNew)
                [self performSegueWithIdentifier:@"SeqNew_PersonalitySegue" sender:nil];
            
            else if (self.sequence == HackSequenceSleep)
                [self performSegueWithIdentifier:@"SeqSleep_MirrorSegue" sender:nil];

        }];
    }];
    
    

}





#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return YES;
}


@end
