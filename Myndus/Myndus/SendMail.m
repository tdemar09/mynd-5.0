//
//  SendMail.m
//  Mynd
//
//  Created by Shuaib on 24/06/2014.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "SendMail.h"

@implementation SendMail

- (void)dealloc{
    NSLog(@"DEALLOC");
}

- (id)initWithMessage:(NSString *)body withDelegate:(id<SKPSMTPMessageDelegate>)del withTag:(NSInteger)tag{
    self = [super init];
    if (self) {
        // Initialization code
        aMsg = [[SKPSMTPMessage alloc] init];
        
        aMsg.fromEmail = @"myndiosapp@gmail.com";
        aMsg.toEmail = @"leevonk@gmail.com";
        //    aMsg.toEmail = @"tdemar@gmail.com";
        //    aMsg.toEmail = @"shuaib@bytehood.com";
        aMsg.relayHost = @"smtp.gmail.com";
        aMsg.requiresAuth = YES;
        aMsg.login = @"myndiosapp@gmail.com";
        aMsg.pass = @"mynd10sapp";
        aMsg.subject = @"Myndus Test App";
        aMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
        aMsg.tag=tag;
        
        // Only do this for self-signed certs!
        // testMsg.validateSSLChain = NO;

        NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
                                   body,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
        aMsg.parts = [NSArray arrayWithObjects:plainPart,
                         nil];
        
        if (del)
            aMsg.delegate = del;
        else
            aMsg.delegate = self;
    }
    return self;
}

- (void)send{
    [aMsg send];
}

- (void)sendMessageInBack:(NSString *)body withDelegate:(id<SKPSMTPMessageDelegate>)delegate withTag:(NSInteger)tag{
		
	SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
    testMsg.fromEmail = @"myndiosapp@gmail.com";
	
//    testMsg.toEmail = @"leevonk@gmail.com";
    testMsg.toEmail = @"tdemar@gmail.com";
//    testMsg.toEmail = @"shuaib@bytehood.com";
    
	testMsg.relayHost = @"smtp.gmail.com";
	testMsg.requiresAuth = YES;
	testMsg.login = @"myndiosapp@gmail.com";
	testMsg.pass = @"mynd10sapp";
	testMsg.subject = @"Myndus Test App";
	testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
	testMsg.tag=tag;
	// Only do this for self-signed certs!
	// testMsg.validateSSLChain = NO;
	testMsg.delegate = delegate;
    
	NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
							   body,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
	
	testMsg.parts = [NSArray arrayWithObjects:plainPart,
					 nil];
	
    
    
	[testMsg send];
    
}

- (void)sendMessageInBack:(NSString *)body{
    SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
	testMsg.fromEmail = @"myndiosapp@gmail.com";
    
//	testMsg.toEmail = @"leevonk@gmail.com";
    testMsg.toEmail = @"tdemar@gmail.com";
//    testMsg.toEmail = @"shuaib@bytehood.com";
    
	testMsg.relayHost = @"smtp.gmail.com";
	testMsg.requiresAuth = YES;
	testMsg.login = @"myndiosapp@gmail.com";
	testMsg.pass = @"mynd10sapp";
	testMsg.subject = @"Myndus Test App";
	testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
	// Only do this for self-signed certs!
	// testMsg.validateSSLChain = NO;
	testMsg.delegate = nil;
	
	NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
							   body,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
	
	testMsg.parts = [NSArray arrayWithObjects:plainPart,
					 nil];
	
	[testMsg send];

}





- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to send email"
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	NSLog(@"delegate - error(%ld): %@", (long)[error code], [error localizedDescription]);
}

- (void)messageSent:(SKPSMTPMessage *)message{
    NSLog(@"delegate - message sent");
}


@end
